package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const (
	// GbAPIKey - GiantBomb API key to access the wiki
	GbAPIKey = "e8a3ee2589f8db0821210188ca6e7181f83ac104"
	// GbBaseURL - GiantBomb base url
	GbBaseURL = "https://www.giantbomb.com/api/"
)

func getGame(w http.ResponseWriter, r *http.Request) {
	game := r.URL.Query()

	gameName, present := game["game"]

	if !present {
		fmt.Println("Error: no game was provided")

		w.WriteHeader(404)
		w.Write([]byte("No game provided!"))

	} else {
		fmt.Println(gameName)
		resp, err := http.Get(strings.Join([]string{GbBaseURL, "games/?api_key=", GbAPIKey, "&name=", gameName[0]}, ""))

		if err != nil {
			w.WriteHeader(404)
			w.Write([]byte("GB Error!"))
			fmt.Println(err.Error())
		} else {
			defer resp.Body.Close()

			body, err := ioutil.ReadAll(resp.Body)

			if err != nil {
				fmt.Println("Shit got bad")
				w.WriteHeader(500)
				w.Write([]byte("Error!"))
			} else {
				fmt.Println(string(body))
				w.WriteHeader(200)
				w.Write([]byte(body))
			}
		}

	}
}

func main() {
	fmt.Println("Starting Game Genie...")

	http.HandleFunc("/game", getGame)

	http.ListenAndServe(":60543", nil)
}
