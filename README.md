# GAME GENIE

## REASON FOR EXISTENCE

The Game Genie API allows you to submit a game that you like, and through scanning the internet for descriptions and reviews of many games will provide you recommendations based on the output of a String similarity algorithm

Game Genie is offered under a BSD license as detailed in the accompanying LICENSE file
